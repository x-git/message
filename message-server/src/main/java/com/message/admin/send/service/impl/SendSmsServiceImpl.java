package com.message.admin.send.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.message.admin.send.dao.SendSmsDao;
import com.message.admin.send.enums.SendStatus;
import com.message.admin.send.pojo.SendSms;
import com.message.admin.send.service.SendSmsService;
import com.system.comm.model.Page;
import com.system.comm.utils.FrameNoUtil;
import com.system.comm.utils.FrameStringUtil;
import com.system.comm.utils.FrameTimeUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * send_sms的Service
 * @author autoCode
 * @date 2017-12-13 11:15:58
 * @version V1.0.0
 */
@Component
public class SendSmsServiceImpl implements SendSmsService {

	@Autowired
	private SendSmsDao sendSmsDao;
	
	@Override
	public ResponseFrame saveList(String msgId, String content, String phones,
			Date sendTime) {
		ResponseFrame frame = new ResponseFrame();
		if(FrameStringUtil.isEmpty(phones)) {
			frame.setCode(-2);
			frame.setMessage("不存在接收手机号");
			return frame;
		}
		List<String> phoneList = FrameStringUtil.toArray(phones, ";");
		if(phoneList.size() == 0) {
			frame.setCode(-2);
			frame.setMessage("不存在接收手机号");
			return frame;
		}
		for (String phone : phoneList) {
			SendSms sendSms = new SendSms();
			sendSms.setMsgId(msgId);
			sendSms.setContent(content);
			sendSms.setPhone(phone);
			sendSms.setSendTime(sendTime);
			save(sendSms);
		}
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}
	
	private void save(SendSms sendSms) {
		sendSms.setId(FrameNoUtil.uuid());
		if(sendSms.getStatus() == null) {
			sendSms.setStatus(SendStatus.WAIT.getCode());
		}
		sendSms.setCreateTime(FrameTimeUtil.getTime());
		sendSmsDao.save(sendSms);
	}

	@Override
	public SendSms get(String id) {
		return sendSmsDao.get(id);
	}

	@Override
	public ResponseFrame pageQuery(SendSms sendSms) {
		sendSms.setDefPageSize();
		ResponseFrame frame = new ResponseFrame();
		int total = sendSmsDao.findSendSmsCount(sendSms);
		List<SendSms> data = null;
		if(total > 0) {
			data = sendSmsDao.findSendSms(sendSms);
		}
		Page<SendSms> page = new Page<SendSms>(sendSms.getPage(), sendSms.getSize(), total, data);
		frame.setBody(page);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}
	
	@Override
	public ResponseFrame delete(String id) {
		ResponseFrame frame = new ResponseFrame();
		sendSmsDao.delete(id);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public void updateWaitToIng(String servNo, Integer num) {
		sendSmsDao.updateWaitToIng(servNo, num);
	}

	@Override
	public List<SendSms> findIng(String servNo) {
		return sendSmsDao.findIng(servNo);
	}

	@Override
	public void updateStatus(String id, Integer status) {
		sendSmsDao.updateStatus(id, status);
	}
}