package com.message.ui.sys.service;

import org.springframework.stereotype.Component;

import com.message.ui.sys.pojo.AdminUser;
import com.system.handle.model.ResponseFrame;

/**
 * admin_user的Service
 * @author autoCode
 * @date 2017-12-27 15:24:05
 * @version V1.0.0
 */
@Component
public interface AdminUserService {
	
	/**
	 * 保存或修改
	 * @param adminUser
	 * @return
	 */
	public ResponseFrame saveOrUpdate(AdminUser adminUser);
	
	/**
	 * 根据id获取对象
	 * @param id
	 * @return
	 */
	public AdminUser get(String id);

	/**
	 * 分页获取对象
	 * @param adminUser
	 * @return
	 */
	public ResponseFrame pageQuery(AdminUser adminUser);
	
	/**
	 * 根据id删除对象
	 * @param id
	 * @return
	 */
	public ResponseFrame delete(String id);

	public ResponseFrame login(AdminUser adminUser);
}