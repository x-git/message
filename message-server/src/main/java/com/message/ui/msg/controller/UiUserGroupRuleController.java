package com.message.ui.msg.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.message.admin.sys.service.SysInfoService;
import com.message.admin.sys.service.UserGroupRuleService;
import com.message.admin.sys.service.UserInfoService;
import com.message.ui.comm.controller.BaseController;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * userInfo的Controller
 * @author autoCode
 * @date 2017-12-27 15:24:04
 * @version V1.0.0
 */
@Controller
public class UiUserGroupRuleController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UiUserGroupRuleController.class);

	@Autowired
	private UserGroupRuleService userGroupRuleService;
	@Autowired
	private UserInfoService userInfoService;
	@Autowired
	private SysInfoService sysInfoService;
	
	@RequestMapping(value = "/userGroupRule/f-view/manager")
	public String manger(HttpServletRequest request, ModelMap modelMap, String id) {
		modelMap.put("sysInfos", sysInfoService.findKvAll());
		modelMap.put("userInfo", userInfoService.get(id));
		return "admin/msg/userGroupRule-manager";
	}

	/**
	 * 分页获取信息
	 * @return
	 */
	@RequestMapping(value = "/userGroupRule/f-json/find")
	@ResponseBody
	public void pageQuery(HttpServletRequest request, HttpServletResponse response,
			String sysNo, String userId) {
		ResponseFrame frame = new ResponseFrame();
		try {
			frame.setBody(userGroupRuleService.findBySysNoUserId(sysNo, userId));
			frame.setSucc();
		} catch (Exception e) {
			LOGGER.error("分页获取信息异常: " + e.getMessage(), e);
			frame.setCode(ResponseCode.FAIL.getCode());
			frame.setMessage(ResponseCode.FAIL.getMessage());
		}
		writerJson(response, frame);
	}
	
	@RequestMapping(value = "/userGroupRule/f-view/edit")
	public String edit(HttpServletRequest request, ModelMap modelMap,
			String userId, String sysNo, String groupId) {
		/*if(FrameStringUtil.isNotEmpty(id)) {
			modelMap.put("msgGroup", userGroupRuleService.get(id, sysNo));
		}*/
		modelMap.put("sysInfos", sysInfoService.findKvAll());
		return "admin/msg/userGroupRule-edit";
	}
}