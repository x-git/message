<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/view/inc/sys.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>定时任务-编辑用户分组规则</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body class="cld-body">
	<div class="enter-panel ep-xs">
  		<div class="form-group">
			<label for="smsStatus" class="col-sm-4">接收短信</label>
			<div class="col-sm-8">
				<my:radio name="smsStatus" dictcode="user_group_rule_status" defvalue="10" value="${userGroupRule.smsStatus}"/>
			</div>
		</div>
  		<div class="form-group">
			<label for="emailStatus" class="col-sm-4">接收邮件</label>
			<div class="col-sm-8">
				<my:radio name="emailStatus" dictcode="user_group_rule_status" defvalue="10" value="${userGroupRule.emailStatus}"/>
			</div>
		</div>
  		<div class="form-group">
			<label for="name" class="col-sm-4">接收手机</label>
			<div class="col-sm-8"><input type="text" class="form-control" id="recePhone" placeholder="接收手机[多个;分隔]" value="${userGroupRule.recePhone}"></div>
		</div>
  		<div class="form-group">
			<label for="name" class="col-sm-4">接收邮箱</label>
			<div class="col-sm-8"><input type="text" class="form-control" id="receEmail" placeholder="接收邮箱[多个;分隔]" value="${userGroupRule.receEmail}"></div>
		</div>
		<hr/>
  		<div class="form-group text-right">
			<span id="saveMsg" class="label label-danger"></span>
 			<div class="btn-group">
				<button type="button" id="saveBtn" class="btn btn-success enter-fn">保存</button>
			</div>
		</div>
	</div>

	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<script type="text/javascript">
	$(function() {
		$('#saveBtn').click(function() {
			var _saveMsg = $('#saveMsg').empty();
			
			var _sysNo = $('#sysNo');
			if(JUtil.isEmpty(_sysNo.val())) {
				_saveMsg.append('请选择系统');
				_sysNo.focus();
				return;
			}
			var _id = $('#id');
			if(JUtil.isEmpty(_id.val())) {
				_saveMsg.append('请输入编码');
				_id.focus();
				return;
			}
			var _name = $('#name');
			if(JUtil.isEmpty(_name.val())) {
				_saveMsg.append('请输入名称');
				_name.focus();
				return;
			}
			var _type = $('#type');
			if(JUtil.isEmpty(_type.val())) {
				_saveMsg.append('请输入类型');
				_type.focus();
				return;
			}
			var _pid = $('#pid');
			if(JUtil.isEmpty(_pid.val())) {
				_saveMsg.append('请输入父编码');
				_pid.focus();
				return;
			}


			var _saveBtn = $('#saveBtn');
			var _orgVal = _saveBtn.html();
			_saveBtn.attr('disabled', 'disabled').html('保存中...');
			JUtil.ajax({
				url : '${webroot}/userGroupRule/f-json/save',
				data : {
					id: _id.val(),
					sysNo: _sysNo.val(),
					name: _name.val(),
					type: _type.val(),
					pid: _pid.val(),
				},
				success : function(json) {
					if (json.code === 0) {
						_saveMsg.attr('class', 'label label-success').append('保存成功');
						setTimeout(function() {
							parent.info.loadInfo();
							parent.dialog.close();
						}, 800);
					}
					else if (json.code === -1)
						_saveMsg.append(JUtil.msg.ajaxErr);
					else
						_saveMsg.append(json.message);
					_saveBtn.removeAttr('disabled').html(_orgVal);
				}
			});
		});
	});
	</script>
</body>
</html>