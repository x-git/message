<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="my" uri="/WEB-INF/tld/my.tld" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${projectName}-用户分组规则列表</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="/WEB-INF/view/inc/header.jsp"></jsp:include>
	<div class="container">
		<jsp:include page="/WEB-INF/view/admin/comm/left.jsp">
			<jsp:param name="first" value="msg"/>
			<jsp:param name="second" value="userInfoManager"/>
		</jsp:include>
		<div class="c-right">
			<div class="panel panel-success">
				<div class="panel-heading panel-heading-tool">
					<div class="row">
						<div class="col-sm-5 title">消息管理 / <b>系统分组列表</b></div>
						<div class="col-sm-7 text-right">
							<div class="btn-group">
						  		<a href="${webroot}/userInfo/f-view/manager" class="btn btn-default btn-sm">返回</a>
						  		<a href="javascript:location.reload()" class="btn btn-default btn-sm">刷新</a>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body">
				  	<div class="table-tool-panel">
						<div class="row">
							<div class="col-sm-6 enter-panel">
								<div class="btn-group">
								<my:select id="sysNo" items="${sysInfos}" cssCls="form-control input-sm" value="${userInfo.sysNo}" exp="style=\"width:120px;\""/>
								</div>
								<!-- <input type="text" style="width: 200px;display: inline;" class="form-control input-sm" id="userId" placeholder="根据用户编码搜索" value=""> -->
							  	<button type="button" class="btn btn-sm btn-default enter-fn" onclick="info.loadInfo()">查询</button>
							</div>
							<div class="col-sm-6 text-right">
							  	<!-- <div class="btn-group">
							  		<a href="javascript:;" class="btn btn-success btn-sm" onclick="info.edit()">新增系统分组</a>
							  	</div> -->
							</div>
						</div>
				  	</div>
					<div id="infoPanel" class="table-panel"></div>
					<!-- <div id="infoPage" class="table-page-panel"></div> -->
				</div>
			</div>
		</div>
		<br clear="all">
	</div>
	<jsp:include page="/WEB-INF/view/inc/footer.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/view/inc/utils/page.jsp"></jsp:include>
<script type="text/javascript">
var info = {
		//获取用户信息
		loadInfo : function() {
			var _infoPanel = $('#infoPanel').empty();
			JUtil.ajax({
				url : '${webroot}/userGroupRule/f-json/find',
				data : { userId: '${userInfo.userId}', sysNo: $('#sysNo').val() },
				error : function(json){ _infoPanel.append('加载信息出错了!'); },
				success : function(json){
					if(json.code === 0) {
						var _cont = ['<table class="table table-striped table-hover"><thead><tr class="info">',
						                         '<th>编码</th>',
						                         '<th>名称</th>',
						                         '<th>接收状态</th>',
						                         '<th>接收邮件</th>',
						                         '<th>接收短信</th>',
						                         '<th width="120">操作</th>',
						                         '</tr></thead><tbody>'];
						$.each(json.body, function(i, obj) {
							var _status = ['<input type="checkbox" ',(obj.status==10) ? 'checked':'','>'];
							var _emailStatus = ['<input type="checkbox" ',(obj.emailStatus==10) ? 'checked':'','>'];
							var _smsStatus = ['<input type="checkbox" ',(obj.smsStatus==10) ? 'checked':'','>'];
							_cont.push('<tr>',
								    	'<td>',obj.groupId,'</td>',
								    	'<td>',obj.groupName,'</td>',
								    	'<td>',_status.join(''),'</td>',
								    	'<td>',_emailStatus.join(''),'</td>',
								    	'<td>',_smsStatus.join(''),'</td>',
								    	'<td>',
								    	'<a class="glyphicon text-success" href="javascript:info.edit(\'',obj.sysNo,'\',\'',obj.groupId,'\')" title="设置接收手机和邮箱">设置手机和邮箱</a>',
								    	//'&nbsp; <a class="glyphicon glyphicon-remove text-success" href="javascript:info.del(\'',obj.id,'\',\'',obj.sysNo,'\')" title="删除"></a>',
								    	'</td>',
									'</tr>');
						});
						_cont.push('</tbody></table>');
						_infoPanel.append(_cont.join(''));
					}
					else alert(JUtil.msg.ajaxErr);
				}
			});
		},
		//编辑
		edit : function(sysNo, groupId) {
			dialog({
				title: '编辑用户分组规则',
				url: webroot + '/userGroupRule/f-view/edit?userId=${userInfo.userId}&sysNo='+sysNo+'&groupId='+groupId,
				type: 'iframe',
				width: 420,
				height: 320
			});
		}
};
$(function() {
	info.loadInfo();
});
</script>
</body>
</html>